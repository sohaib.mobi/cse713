# Task 1: Indivdual Assignment
## Topic: Clustering using OpenRefine

### Submission By
#### Name: A M Sohaib Reza
##### ID: 19166017
##### Email: a.m.sohaib.reza@g.bracu.ac.bd

### Links
- YouTube: [YouTube](https://youtu.be/kVJqoAezK_Q)
- Dataset: [LibraryCarpentry](https://github.com/LibraryCarpentry/lc-open-refine/raw/gh-pages/data/doaj-article-sample.csv)
- Data File: [data.csv](data.csv)

